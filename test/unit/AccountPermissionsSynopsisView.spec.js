import AccountPermissionsSynopsisView from '../../src/accountPermissionsSynopsisView';
import dummy from '../dummy';

describe('AccountPermissionsSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AccountPermissionsSynopsisView(
                    null,
                    dummy.accountId,
                    dummy.spiff,
                    dummy.extendedWarranty,
                    dummy.startDate,
                    dummy.endDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new AccountPermissionsSynopsisView(
                    expectedId,
                    dummy.accountId,
                    dummy.spiff,
                    dummy.extendedWarranty,
                    dummy.startDate,
                    dummy.endDate
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    null,
                    dummy.spiff,
                    dummy.extendedWarranty,
                    dummy.startDate,
                    dummy.endDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountId required');
        });

        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedAccountId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    expectedAccountId,
                    dummy.spiff,
                    dummy.extendedWarranty,
                    dummy.startDate,
                    dummy.endDate
                );

            /*
             assert
             */
            const actualAccountId = objectUnderTest.accountId;
            expect(actualAccountId).toEqual(expectedAccountId);

        });

        it('throws if spiff is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    dummy.accountId,
                    null,
                    dummy.extendedWarranty,
                    dummy.startDate,
                    dummy.endDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiff required');
        });

        it('sets spiff', () => {
            /*
             arrange
             */
            const expectedSpiff = dummy.spiff;

            /*
             act
             */
            const objectUnderTest =
                new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    dummy.accountId,
                    expectedSpiff,
                    dummy.extendedWarranty,
                    dummy.startDate,
                    dummy.endDate
                );

            /*
             assert
             */
            const actualSpiff = objectUnderTest.spiff;
            expect(actualSpiff).toEqual(expectedSpiff);

        });

        it('throws if extendedWarranty is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    dummy.accountId,
                    dummy.spiff,
                    null,
                    dummy.startDate,
                    dummy.endDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'extendedWarranty required');
        });

        it('sets extendedWarranty', () => {
            /*
             arrange
             */
            const expectedExtendedWarranty = dummy.extendedWarranty;

            /*
             act
             */
            const objectUnderTest =
                new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    dummy.accountId,
                    dummy.spiff,
                    expectedExtendedWarranty,
                    dummy.startDate,
                    dummy.endDate
                );

            /*
             assert
             */
            const actualExtendedWarranty = objectUnderTest.extendedWarranty;
            expect(actualExtendedWarranty).toEqual(expectedExtendedWarranty);

        });

        it('throws if startDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>  new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    dummy.accountId,
                    dummy.spiff,
                    dummy.extendedWarranty,
                    null,
                    dummy.endDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'startDate required');
        });

        it('sets startDate', () => {
            /*
             arrange
             */
            const expectedStartDate = dummy.startDate;

            /*
             act
             */
            const objectUnderTest =
                new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    dummy.accountId,
                    dummy.spiff,
                    dummy.extendedWarranty,
                    expectedStartDate,
                    dummy.endDate
                );

            /*
             assert
             */
            const actualStartDate = objectUnderTest.startDate;
            expect(actualStartDate).toEqual(expectedStartDate);

        });

        it('throws if endDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>  new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    dummy.accountId,
                    dummy.spiff,
                    dummy.extendedWarranty,
                    dummy.startDate,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'endDate required');
        });

        it('sets endDate', () => {
            /*
             arrange
             */
            const expectedEndDate = dummy.endDate;

            /*
             act
             */
            const objectUnderTest =
                new AccountPermissionsSynopsisView(
                    dummy.accountPermissionsId,
                    dummy.accountId,
                    dummy.spiff,
                    dummy.extendedWarranty,
                    dummy.startDate,
                    expectedEndDate
                );

            /*
             assert
             */
            const actualEndDate = objectUnderTest.endDate;
            expect(actualEndDate).toEqual(expectedEndDate);

        });

    })
});

