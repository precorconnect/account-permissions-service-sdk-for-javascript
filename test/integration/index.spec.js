import AccountPermissionsServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be AccountPermissionsServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new AccountPermissionsServiceSdk(config.accountPermissionsServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(AccountPermissionsServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('getAccountPermissionsWithId method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new AccountPermissionsServiceSdk(config.accountPermissionsServiceSdkConfig);


                /*
                 act
                 */
                const accountPermissionsPromise =
                    objectUnderTest
                        .getAccountPermissionsWithId(
                            dummy.accountId,
                            factory
                                .constructValidPartnerRepOAuth2AccessToken()
                        );
                /*
                 assert
                 */
                accountPermissionsPromise
                    .then((AccountPermissionsSynopsisView) => {
                        if(AccountPermissionsSynopsisView)
                            expect(AccountPermissionsSynopsisView).toBeTruthy();
                        else
                            expect(AccountPermissionsSynopsisView).toBeNull();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000)
        });
    });
});