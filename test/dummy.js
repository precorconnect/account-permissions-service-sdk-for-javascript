/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    firstName: 'firstName',
    lastName: 'lastName',
    accountPermissionsId:1,
    repAccountId:'000000000000000000',
    sapVendorNumber:'0000000000',
    userId:'email@test.com',
    url:'https://dummy-url.com',
    accountId:'161018',
    spiff:true,
    extendedWarranty:true,
    startDate:'1/1/2016',
    endDate:'12/31/2099'
};